#!/usr/bin/env bash

# Helper script to generate ASAP Auth token for HTTP requests and other use cases
# Run by ./asap-generate-token.sh -e ENV -s SERVICE -t EXPIRY
# Requires micros-cli and asap-cli installed on your machine

set -e

checkCommandExists () {
  if ! type "$1" > /dev/null ; then
    echo "Command '$1' does not exist. You can install it by running:"
    echo ""
    echo "  $2"
    echo ""
    exit 1
  fi
}

printHelp () {
  echo "
  Usage:
  $(basename $0) [-e env] [-s service] [-t expiry] [-a audience] -- Generate ASAP Bearer token for testing purposes
  
  Flags:
    -e  Environment to assume role (e.g. stg-east)
    -s  Service name (e.g. pf-content-service)
    -t  Expiration time in seconds (Defaults to 3600)
    -a  Audience (defaults to audience returned by micros command)
  "
}

checkCommandExists asap "npm install -g asap-cli"
checkCommandExists "atlas" "brew install atlas-cli  ( see https://hello.atlassian.net/wiki/spaces/MICROS/pages/536392995/HOWTO+Log+in+to+Micros+with+Atlas+CLI )"

atlas micros cli heartbeat > /dev/null

EXPIRY=3600

while getopts ":e:s:a:t:h" opt; do
  case ${opt} in
    h )
      printHelp
      exit 0
      ;;
    t )
      EXPIRY=$OPTARG
      ;;
    e )
      ENVIRONMENT=$OPTARG
      ;;
    s )
      SERVICE=$OPTARG
      ;;
    a )
      AUDIENCE_OVERRIDE=$OPTARG
      ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      printHelp
      exit 1
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument" 1>&2
      printHelp
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))

if [ -z "$SERVICE" ] || [ -z "$ENVIRONMENT" ]; then
  printHelp
  exit 1
fi

MICROS=$(atlas micros service:assume:role "$SERVICE" -e "$ENVIRONMENT")

while read line
do
  if [[ $line == "ASAP_PRIVATE_KEY="* ]]; then
    ASAP_PRIVATE_KEY=${line/ASAP_PRIVATE_KEY=/""}
  fi
  if [[ $line == "ASAP_ISSUER="* ]]; then
    ASAP_ISSUER=${line/ASAP_ISSUER=/""}
  fi
  if [[ $line == "ASAP_AUDIENCE="* ]]; then
    ASAP_AUDIENCE=${line/ASAP_AUDIENCE=/""}
  fi
  if [[ $line == "ASAP_KEY_ID="* ]]; then
    ASAP_KEY_ID=${line/ASAP_KEY_ID=/""}
  fi
done < <(printf '%s\n' "$MICROS")

if [ -n "$AUDIENCE_OVERRIDE" ]; then
  ASAP_AUDIENCE=$AUDIENCE_OVERRIDE
fi

asap --private-key "$ASAP_PRIVATE_KEY" --issuer "$ASAP_ISSUER" --audience "$ASAP_AUDIENCE" --kid "$ASAP_KEY_ID" --expiry "$EXPIRY" token | tr -d '\n'
